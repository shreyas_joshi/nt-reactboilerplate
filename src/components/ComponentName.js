import React from "react";
import "./ComponentName.css";

class ComponentName extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isHovered: false
    };
    this.onEnter = this.onEnter.bind(this);
    this.onLeave = this.onLeave.bind(this);
  }

  onEnter() {
    this.setState({
      isHovered: true
    });
  }

  onLeave() {
    this.setState({
      isHovered: false
    });
  }

  render() {
    return (
      // eslint-disable-next-line
      <a
        onMouseEnter={this.onEnter}
        onMouseLeave={this.onLeave}
        to={this.props.linkTo}
        className="nt-btn"
        href="#"
        style={{
          backgroundColor: this.state.isHovered
            ? "#555555"
            : this.props.bgColor
              ? this.props.color
              : "#FFFFFF",
          color: this.state.isHovered
            ? "#FFFFFF"
            : this.props.textColor
              ? this.props.textColor
              : "#555555",
          border: this.state.isHovered ? "2px solid #555555" : "none"
        }}
      >
        {this.props.children}
      </a>
    );
  }
}

export default ComponentName;
